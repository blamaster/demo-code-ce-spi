/*#############################################################################
 *###
 *###   Demo for CE related to SPI
 *###   ===========
 *#############################################################################
 *
 * Code belongs to TI4 CE; Informatik; HAW-Hamburg; Berliner Tor 7; D-20099 Hamburg
 * Code is based on demo examples from Silke Behn, Heiner Heitmann, Jan Quenzel & Bernd Schwarz
 * Code was adapted/ported to Silica Xynergy Board by Yannic Wilkening and Florian Meyer
 *
 *-----------------------------------------------------------------------------
 * Description:
 * ============
 * Code is an example for simple SPI application.
 * "Manufacturer and Device ID" is requested from AT25DF641 flash memory.
 *
 *-----------------------------------------------------------------------------
 * Abbreviations:
 * ==============
 *
 * RM ::= Reference Manual RM0090 ; DM00031020 ;  DocID 018909 Rev9 ;  2015-03
 *
 *-----------------------------------------------------------------------------
 * History:
 * ========
 *   150518: code ported to Silica Xynergy Board by Yannic Wilkening & Florian Meyer
 *   For "full history" see ReadMe.txt
 *-----------------------------------------------------------------------------
 */

#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_spi.h>
#include <stdint.h>
#include "CE_Lib.h"
#include "tft.h"

// current version of sample code
#define VERSION "V1.00" 

// comment line if you dont want to use LCD info
#define USE_LCD 

// config spi mode
#define SPI_MODE_3			(SPI_CPOL_High | SPI_CPHA_2Edge)    // set spi mode 3 for AT25DF641 ()
#define SPI_CLK_DIV_2		SPI_BaudRatePrescaler_2             // 

// AT25DF641 OPCODES
#define OP_READ_DEVICE_ID	0x9F                                // opcode for read device id (AT25DF641 - 3680F)


// macro converts binary value (containing up to 8 bits resp. <=0xFF) to unsigned decimal value
// as substitute for missing 0b prefix for binary coded numeric literals.
// macro does NOT check for an invalid argument at all.
#define b(n) (                                               \
    (unsigned char)(                                         \
    ( ( (0x##n##ul) & 0x00000001 )  ?  0x01  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00000010 )  ?  0x02  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00000100 )  ?  0x04  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00001000 )  ?  0x08  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00010000 )  ?  0x10  :  0 )  \
    |   ( ( (0x##n##ul) & 0x00100000 )  ?  0x20  :  0 )  \
    |   ( ( (0x##n##ul) & 0x01000000 )  ?  0x40  :  0 )  \
    |   ( ( (0x##n##ul) & 0x10000000 )  ?  0x80  :  0 )  \
    )                                                        \
    )

// simplified acces to switches S0 - S7
#define  S1   ( !(GPIOH->IDR & (1 << 15)) )
#define  S2   ( !(GPIOH->IDR & (1 << 12)) )
#define  S3   ( !(GPIOH->IDR & (1 << 10)) )
#define  S4   ( !(GPIOF->IDR & (1 << 8 )) )
#define  S5   ( !(GPIOF->IDR & (1 << 7 )) )
#define  S6   ( !(GPIOF->IDR & (1 << 6 )) )
#define  S7   ( !(GPIOC->IDR & (1 << 2 )) )
#define  S8   ( !(GPIOI->IDR & (1 << 9 )) )
		
		
// SPI helper

uint8_t spi_read_byte(void) {
	SPI3->DR = 0xFF;                    // write dummy byte into data register
	while(!(SPI3->SR & SPI_SR_RXNE));   // wait until valid data is in rx buffer (RM0090 Chap 28.3.7)
	return SPI3->DR;                    // read data register (RM0090 Chap 28.3.7)
}

uint8_t spi_write_byte(uint8_t data) {
	SPI3->DR = data;                    // write dummy byte into data register
	while(!(SPI3->SR & SPI_SR_RXNE));   // wait until valid data is in rx buffer (RM0090 Chap 28.3.7)
	return SPI3->DR;                    // read data register (RM0090 Chap 28.3.7)
}

//----------------------------------------------------------------------------
//
//  MAIN
//
int main( void ){
	char lcdBuff[9];
	uint32_t deviceInfo = 0;;
	
    // general setup
    // =============
    //
    initCEP_Board();                                // initilize display, leds, buttons, uart and other stuff

    // clock setup (enable clock for used ports)
    // =============
    //
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;            // enable clock for GPIOA (RM0090 Chap 6.3.10)
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;            // enable clock for GPIOB
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOCEN;            // enable clock for GPIOC
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOFEN;            // enable clock for GPIOF
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOGEN;            // enable clock for GPIOG
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOHEN;            // enable clock for GPIOH
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOIEN;            // enable clock for GPIOI

    // SPI
    // =====
    //
	// Set PortB.9 as output (Chip Select 1)		
	GPIOB->MODER |= (GPIO_Mode_OUT << (2*9)); 
	// Set PortG.6 as output (Chip Select 2)						
	GPIOG->MODER |= (GPIO_Mode_OUT << (2*6));
    
	// Set SPI Pins to alternate port function
    // set IO mode to alternate function (RM0090 Chap 8.3.2)
	GPIOC->MODER |= (GPIO_Mode_AF << (2*10)) | (GPIO_Mode_AF << (2*11)) | (GPIO_Mode_AF << (2*12));
	GPIOC->OSPEEDR |= (GPIO_Fast_Speed << (2*10)) | (GPIO_Fast_Speed << (2*11)) | (GPIO_Fast_Speed << (2*12));
	// set alternate function mode for use with spi (RM0090 Chap 8.3.2)
    GPIOC->AFR[1] |= (GPIO_AF_SPI3 << (4*2)) | (GPIO_AF_SPI3 << (4*3)) | (GPIO_AF_SPI3 << (4*4));
    
	// Set SPI Configuration
	// enable clock for SPI (RM0090 Chap 6.3.13)
	RCC->APB1ENR |= RCC_APB1ENR_SPI3EN;
	// set SPI configuration(RM0090 Chap 28.5.1)    
	SPI3->CR1 = (SPI_CR1_SPE | SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI | SPI_CLK_DIV_2 | SPI_MODE_3);

    // LCD info
    // ==============================
    //
#ifdef USE_LCD
    TFT_cls();
    TFT_gotoxy(1,1);
    TFT_puts("[Demo for CE related to A5]");
    TFT_gotoxy(1,2);
    TFT_puts(VERSION);
#endif

	// dsiable chip selects
	GPIOB->BSRRL = GPIO_Pin_9;
	GPIOG->BSRRL = GPIO_Pin_6;
    
    // set CS 1
	GPIOB->BSRRH = GPIO_Pin_9;
    
	// write opcode and receive data over spi
    spi_write_byte(OP_READ_DEVICE_ID);
	deviceInfo = (
		spi_read_byte()         |
		(spi_read_byte() << 8)  |
		(spi_read_byte() << 16) |
		(spi_read_byte() << 24)
	);
	
    // reset CS 1
	GPIOB->BSRRL = GPIO_Pin_9;
    
	// print result
	TFT_gotoxy(1,4);
	TFT_puts("Manufacturer and Device ID Information:");
	snprintf(lcdBuff, sizeof(lcdBuff), "%08X", deviceInfo);
	TFT_gotoxy(1,5);
	TFT_puts(lcdBuff);
    
	// NEVER stop doing task
    while(1){};

}//main


